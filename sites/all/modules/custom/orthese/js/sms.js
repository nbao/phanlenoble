/**
 * Created by PhanLenoble on 30/12/2015.
 */
(function ($) {
    Drupal.behaviors.orthese = {
        attach: function (context, settings) {
            $(".send-sms").on("click", function () {
                var post=$(this).data(),
                	type=$(this).data('type');
                var text=$('.view-id-sms .view-header #'+type).text();
                $.each( post, function( key, value ) {
	                if(key=='date'){
		                value = value.replace(' - ',' à ');
	                }
                    text=text.replace('{'+key+'}',value);
                });
                post['message']=text;
	            $(this).removeClass().addClass('btn btn-disabled disabled');
                $.ajax(
                    {
                        url:post.url,
                        type:"POST",
                        data:post,
                        success:function(data){
	                        var url = 'webcal://phanlenoble.fr/generate/calendar.ics?nom='+post['nom']+
								    '&prenom='+post['prenom']+
								    '&portable='+post['portable']+
								    '&title='+post['title']+
								    '&dateplain='+post['dateplain']+
								    '&type='+post['type']+
								    '&lieu='+post['lieu'];
                            console.log(data);
                        },
                        error:function(jqXHR, textStatus, errorMessage)
                        {
                            console.log( textStatus, errorMessage);
                            //error($("#notification"),errorMessage);
                        }
                    });
            });

        }
    }

})(jQuery);