/**
 * Created by baonguyen on 11/11/14.
 */
(function ($) {
    Drupal.behaviors.orthese = {
        attach: function (context, settings) {
            $( "#edit-reset" ).on( "click", function() {
                $(this).closest('form').find('input:text, input:password, select, textarea').val('');
                return false;
            });
	 	$(".field-name-field-facture .field-type-number-float input").bind('change paste keyup',function () {
			  var sum=0;
			  $(".field-name-field-facture .field-type-number-float input").each(function(){
			    sum+=parseFloat($( this ).val());
			  });
			  if(sum){
			    $('.field-name-field-montant-total input').val(sum.toFixed(2));
			  }
		});
        $( "#transmission_pyxvital" ).on( "click", function() {
            var post=$(this).data();
            $( ".file_transfert" ).each(function( index ) {
                var name=$(this).attr('name')
                post[name]=$( this ).val();
            });
            $.ajax(
                {
                    url:post.url,
                    type:"POST",
                    data:post,
                    success:function(vitale){
                        if(typeof vitale !== 'object'){
                            $('pre').html(data);
                        }
                    },
                    error:function(jqXHR, textStatus, errorMessage)
                    {
                        //error($("#notification"),errorMessage);
                    }
                });
		});
          $( ".carte-vitale" ).on( "click", function() {
            $.ajax(
              {
                url:$(this).data('url'),
                dataType: "xml",
                success: function(vitale){
                  if(typeof vitale === 'object'){
                    var person = $(vitale).find("Bénéficiaire")[0];
                    $("#edit-field-n-secu-value").val($(person).find('Numéro').text());
                    $("#edit-field-nom-value").val($(person).find('Nom').text());
                    $("#edit-field-prenom-value").val($(person).find('Prénom').text());

                    $("#edit-field-n-secu-und-0-value").val($(person).find('Numéro').text());
                    $("#edit-field-nom-und-0-value").val($(person).find('Nom').text());
                    $("#edit-field-prenom-und-0-value").val($(person).find('Prénom').text());
                    if(typeof $(person).find('Adresse') !== 'undefined') {
                      var adresse=$(person).find('Adresse').text().split('\\');
                      $("#edit-field-adresse-und-0-value").val(adresse[0]);
                      if(typeof adresse[1] !== 'undefined'){
                        var ville_cp=adresse[1].split(' ');
                        $("#edit-field-code-postal-und-0-value").val(ville_cp.shift());
                        $("#edit-field-ville-und-0-value").val(ville_cp.join(' '));
                      }
                    }

                    var date_naissance=$(person).find('Date_de_naissance').text().split('/');
                    $("#edit-field-date-naissance-und-0-value-day").val(parseInt(date_naissance[0]));
                    $("#edit-field-date-naissance-und-0-value-month").val(parseInt(date_naissance[1]));
                    $("#edit-field-date-naissance-und-0-value-year").val(date_naissance[2]);
                  }
                },
                error:function(jqXHR, textStatus, errorMessage)
                {
                  //error($("#notification"),errorMessage);
                }
              });
          });
        $( "#field-update-vitale-add-more-wrapper" ).on( "click", function() {
            var post=$(this).data();

            $.each(post.command.split(";"), function( index, value ) {
	            var url= post.local+value;
                    console.log(url);
              $.get(url, function( data ) {
                console.log(data);
              });
            });
            $.ajax(
                {
                    url:post.local+post.flux,
                    type:"POST",
                    data:post,
                    success:function(vitale){
                        console.log(vitale);
                        if(typeof vitale !== 'object'){
                           vitale= $.parseXML(vitale);
                        }

                        $("#edit-field-n-secu-value").val(vitale['Numéro']);
                        $("#edit-field-nom-value").val(vitale['Nom']);
                        $("#edit-field-prenom-value").val(vitale['Prénom']);

                        $("#edit-field-n-secu-und-0-value").val(vitale['Numéro']);
                        $("#edit-field-nom-und-0-value").val(vitale['Nom']);
                        $("#edit-field-prenom-und-0-value").val(vitale['Prénom']);
                        if(typeof vitale['Adresse'] !== 'undefined') {
                            var adresse=vitale['Adresse'].split('\\');
                            $("#edit-field-adresse-und-0-value").val(adresse[0]);
                            if(typeof adresse[1] !== 'undefined'){
                                var ville_cp=adresse[1].split(' ');
                                $("#edit-field-code-postal-und-0-value").val(ville_cp.shift());
                                $("#edit-field-ville-und-0-value").val(ville_cp.join(' '));
                            }
                        }

                        var date_naissance=vitale['Date_de_naissance'].split('/');
                        $("#edit-field-date-naissance-und-0-value-day").val(parseInt(date_naissance[0]));
                        $("#edit-field-date-naissance-und-0-value-month").val(parseInt(date_naissance[1]));
                        $("#edit-field-date-naissance-und-0-value-year").val(date_naissance[2]);
                        $("#edit-field-flux-patient input").val(vitale['origin']);

                    },
                    error:function(jqXHR, textStatus, errorMessage)
                    {
                        //error($("#notification"),errorMessage);
                    }
                });
        });

            /* =================================================================
             *  Keep focus on auto-submitted text-fields without surrounding block
             *  ============================================================== */

            function SetCaretAtEnd(elem) {
                var elemLen = elem.value.length;
                // For IE Only
                if (document.selection) {
                    // Set focus
                    elem.focus();
                    // Use IE Ranges
                    var oSel = document.selection.createRange();
                    // Reset position to 0 & then set at end
                    oSel.moveStart('character', -elemLen);
                    oSel.moveStart('character', elemLen);
                    oSel.moveEnd('character', 0);
                    oSel.select();
                }
                else if (elem.selectionStart || elem.selectionStart == '0') {
                    // Firefox/Chrome
                    elem.selectionStart = elemLen;
                    elem.selectionEnd = elemLen;
                    elem.focus();
                } // if
            } // SetCaretAtEnd()

            var textboxToFocus = {};

            jQuery(function($) {
                var addFocusReminder = function(textbox) {
                    textbox.bind('keypress keyup', function(e) {
                        textboxToFocus.formid = $(this).closest('form').attr('id');
                        textboxToFocus.name = $(this).attr('name');

                        if(e.type == 'keypress') {
                            if(e.keyCode != 8) { // everything except return
                                textboxToFocus.value = $(this).val() + String.fromCharCode(e.charCode);
                            } else {
                                textboxToFocus.value = $(this).val().substr(0, $(this).val().length-1)
                            }
                        }
                        else { // keyup
                            textboxToFocus.value = $(this).val();
                        }
                    });
                }

                addFocusReminder($('.view-filters input:text.ctools-auto-submit-processed'));
                $(document).ajaxComplete(function(event,request, settings) {
                    if(typeof textboxToFocus.formid !== 'undefined') {
                        var textBox = $('#' + textboxToFocus.formid + ' input:text[name="' + textboxToFocus.name + '"]');
                        textBox.val(textboxToFocus.value);
                        SetCaretAtEnd(textBox[0]);
                        addFocusReminder(textBox);
                        //textboxToFocus = {}; // if you have other auto-submitted inputs as well
                    }
                });
            });
			$('.dropdown-menu').find('form').click(function (e) {
				e.stopPropagation();
			});
        }
    };
})(jQuery);
