/**
 * Created by ngocbao on 04/01/16.
 */
(function ($) {

    Drupal.behaviors.hmsSelectfield = {
        attach: function (context, settings) {

            // We search in the whole DOM object, not only in our context
            if ($('.hms-field-select-widget').length ) {
                $( ".hms-field-select-widget select" ).change(function() {
                    var hms = [];
                    $(this).closest('.hms-field-select-widget').find("select").each(function() {
                        hms.push($(this).find("option:selected").text());
                    });
                   $(this).closest(".field-type-hms-field").find("input").val(hms.join(':'));
                });
            }

        }
    };

})(jQuery);
