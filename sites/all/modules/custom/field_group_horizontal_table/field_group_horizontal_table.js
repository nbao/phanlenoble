/**
 * Created by ngocbao on 17/01/16.
 */
(function($) {

    Drupal.behaviors.groupHorizontalTable = {
        attach: function (context, settings) {
            $('.field-group-horizontal-table').each(function( index ) {
                var label=$( this).data('labelcol').replace(/ \| /g,'</th><th>');
                $(this).find('thead').append('<tr><th>'+label+'</th></tr>');
            });
        }
    };


})(jQuery);