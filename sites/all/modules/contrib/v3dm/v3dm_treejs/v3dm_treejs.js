(function ($) {

Drupal.behaviors.hop = {
  attach: function (context, settings) {
      // instantiate a loader
      $('.treejs').each(function() {
          var id = $(this).attr('id');
          var loaderName=settings.v3dm_treejs.loader[id];
          let loader = new THREE[loaderName]();
          var canvas = document.getElementById(id);
          var renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});
          renderer.setPixelRatio( window.devicePixelRatio );
          var width = $(this).width();
          var height = $(this).height();
          var file = settings.v3dm_treejs.files[id];
          var scene = new THREE.Scene();
          scene.background = new THREE.Color( parseInt(settings.v3dm_treejs.backgroundColor,16) );
          var camera = new THREE.PerspectiveCamera(75, width / height, 1, 2000);
          camera.position.set( settings.v3dm_treejs.cameraX, settings.v3dm_treejs.cameraY, settings.v3dm_treejs.cameraZ );

          var ambient = new THREE.AmbientLight(0xffffff, 0.3);
          scene.add(ambient);
          keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 75%)'), 1.0);
          keyLight.position.set(-100, 0, 100);

          fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 75%)'), 0.75);
          fillLight.position.set(100, 0, 100);

          backLight = new THREE.DirectionalLight(0xffffff, 1.0);
          backLight.position.set(100, 0, -100).normalize();

          scene.add(keyLight);
          scene.add(fillLight);
          scene.add(backLight);

          var mesh;
          loader.load(file, function (geometry) {
              geometry.computeBoundingBox();
              geometry.computeVertexNormals();
              geometry.center();
              var materiel = { shininess: 20, morphTargets: true, flatShading : THREE.SmoothShading};
              if(settings.v3dm_treejs.ModelColor!='' && settings.v3dm_treejs.SphereMapUrl==''){
                  materiel.color = parseInt(settings.v3dm_treejs.ModelColor,16);
              }
              if(settings.v3dm_treejs.SphereMapUrl!=''){
                  materiel.map = new THREE.TextureLoader().load( settings.v3dm_treejs.SphereMapUrl );
              }
              mesh = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( materiel ) );
              mesh.geometry.computeVertexNormals(true);
              scene.add( mesh );
          });

          // Add OrbitControls so that we can pan around with the mouse.
          var controls = new THREE.OrbitControls(camera, renderer.domElement);
          controls.enableDamping = true;
          controls.dampingFactor = 0.25;
          controls.enableZoom = true;
          render();
          function render() {
              // render using requestAnimationFrame
              requestAnimationFrame(render);
              if ( mesh && settings.v3dm_treejs.InitRotationX!='') {
                  mesh.rotation.x -= 0.01;
              }
              if ( mesh && settings.v3dm_treejs.InitRotationY!='') {
                  mesh.rotation.y -= 0.01;
              }
              if ( mesh && settings.v3dm_treejs.InitRotationZ!='') {
                  mesh.rotation.z -= 0.01;
              }
              renderer.render(scene, camera);
          }
      });

  }
};
}(jQuery));
