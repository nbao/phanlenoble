<?php
/**
 * @file
 * Settings page callback file for the gammu module.
 */

/**
 * Menu callback;
 */

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function gammu_settings($form, &$form_state) {
  $form = array();
  // only administrators can access this function

  // Generate the form - settings applying to all patterns first
  $form['gammu_settings_db'] = array(
    '#type' => 'fieldset',
    '#weight' => -30,
    '#title' => t('Database settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['gammu_settings_db']['gammu_dbhost'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Hostname/IP'),
    '#default_value' => variable_get('gammu_dbhost', 'localhost'),
    '#description' => t("Database Hostname or IP, e.g.: localhost, 192.168.0.1, dbase.drupal.org. Leave blank if you use same Hostname for Drupal and Gammu"),
  );

  $form['gammu_settings_db']['gammu_dbengine'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Engine'),
    '#default_value' => variable_get('gammu_dbengine', 'mysql'),
    '#description' => t("Database Engine, e.g.: mysql, mysqli, pgsql"),
  );

  $form['gammu_settings_db']['gammu_dbport'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Engine'),
    '#default_value' => variable_get('gammu_dbport', '3306'),
    '#description' => t("Database port, e.g.: 3306"),
  );

  $form['gammu_settings_db']['gammu_dbname'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Name'),
    '#default_value' => variable_get('gammu_dbname', 'smsd'),
    '#description' => t("Database Name, e.g.: smsd, drpl_123, mydata. Leave blank if you use same Database Name for Drupal and Gammu"),
  );

  $form['gammu_settings_db']['gammu_dbuser'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Username'),
    '#default_value' => variable_get('gammu_dbuser', ''),
    '#description' => t("Database Username, e.g.: root, user_123. Leave blank if you use same Username for Drupal and Gammu"),
  );

  $form['gammu_settings_db']['gammu_dbpass'] = array(
    '#type' => 'password',
    '#title' => t('Database Password'),
    '#default_value' => variable_get('gammu_dbpass', ''),
    '#description' => t("Database Password, leave blank if you use same database Password for Drupal and Gammu"),
  );

  $form['gammu_settings_advanced'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['gammu_settings_advanced']['gammu_ussd_balance'] = array(
    '#type' => 'textfield',
    '#title' => t('Balance code'),
    '#default_value' => variable_get('gammu_ussd_balance', '*888#'),
    '#description' => t("USSD Code to check Balance/Deposit, e.g.: *888#"),
  );

  $form['gammu_settings_advanced']['gammu_cli'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Gammu CLI'),
    '#default_value' => variable_get('gammu_cli', 0),
    '#description' => t("Use Gammu CLI (Command Line Interface) instead SQL Command. This option requires PHP exec() function."),
  );

  $form['gammu_settings_test'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('Test Gammu'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['gammu_settings_test']['gammu_test_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Your mobile number'),
    '#default_value' => variable_get('gammu_test_number', ''),
    '#description' => t("Enter your mobile number for testing."),
  );

  $form['gammu_settings_test']['gammu_test_text'] = array(
    '#type' => 'textarea',
    '#title' => t('SMS Text'),
    '#rows' => 2,
    '#default_value' => variable_get('gammu_test_text', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),
  );

    // Configuration for phone book
  $form['gammu_settings_phonebook'] = array(
    '#type' => 'fieldset',
    '#title' => t('Phonebook'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Mapping field telephone with first name , lastname'),
    '#prefix' => '<div id="phonebook-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

    $form['gammu_settings_phonebook']['gammu_country_code'] = array(
        '#type' => 'select',
        '#title' => t('Country dial code'),
        '#options' => getCountryDialCode(),
        '#default_value' => variable_get('gammu_country_code', '+33'),
    );

    if (module_exists('telephone') || module_exists('phone')) {
        $options = [false => t("Select field Telephone")];
        foreach (field_info_field_map() as $field_name => $field_info) {
            if (in_array($field_info['type'], ['telephone','phone'])) { //can add type text
                foreach ($field_info['bundles'] as $type_bundles => $bundles) {
                    foreach ($bundles as $bundle) {
                        $field_instance = field_info_instance($type_bundles, $field_name, $bundle);
                        $options["$type_bundles|$field_name|$bundle"] = $field_instance['label'] . " - $bundle";
                    }
                }
            }
        }
        if (!empty($options)) {
            $form['gammu_settings_phonebook']['gammu_telephone_field'] = [
                '#title' => t('Field telephone'),
                '#type' => 'select',
                '#options' => $options,
                '#default_value' => variable_get('gammu_telephone_field', FALSE),
                '#ajax' => [
                    'callback' => 'ajax_select_change_gammu',
                    'wrapper' => 'phonebook-fieldset-wrapper',
                    'method' => 'replace',
                    'effect' => 'fade',
                ],
            ];
            $telephone_field = false;
            if(!empty($form_state['input']['gammu_telephone_field'])){
                $telephone_field =  $form_state['input']['gammu_telephone_field'];
            }elseif(variable_get('gammu_telephone_field', FALSE)){
                $telephone_field = variable_get('gammu_telephone_field');
            }
            if($telephone_field){
                list($type_bundles,$field_name,$bundle)=explode('|',$telephone_field);
            }
            $options = [false => t('Select field')];
            if(!empty($bundle)){
                $field_info = field_info_instances($type_bundles,$bundle);
                foreach ($field_info as $field_name=>$field){
                    $options[$field_name] = $field['label'];
                }
            }
            $form['gammu_settings_phonebook']['gammu_check_status'] = array(
                '#title' => t('Skip check status'),
                '#type' => 'checkbox',
                '#default_value' => variable_get('gammu_check_status', FALSE),
            );
            $form['gammu_settings_phonebook']['gammu_firstname_field'] = array(
                '#title' => t('Frist name'),
                '#type' => 'select',
                '#options' => $options,
                '#default_value' => variable_get('gammu_firstname_field', FALSE),
            );
            $form['gammu_settings_phonebook']['gammu_name_field'] = array(
                '#title' => t('Last name'),
                '#type' => 'select',
                '#options' => $options,
                '#default_value' => variable_get('gammu_name_field', FALSE),
            );
        }
    }else{
        $form['gammu_settings_phonebook']['#description'] = t("Phonebook support")." <a href='https://www.drupal.org/project/telephone'>telephone field</a>".", <a href='https://www.drupal.org/project/phone'>Phone field</a>";
    }

    return system_settings_form($form);
}


function ajax_select_change_gammu(&$form, &$form_state){
    return $form['gammu_settings_phonebook'];
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function gammu_test($form) {
  $gammu_test_number = $form['gammu_settings_test']['gammu_test_number']['#value'];
  $gammu_test_text   = $form['gammu_settings_test']['gammu_test_text']['#value'];

  gammu_sendsms($gammu_test_number, $gammu_test_text);
}

/*
 * Composer and send sms
 */
function gammu_compose($form, &$form_state,$numero=''){
    if($numero == ''){
        $numero = !empty($_GET['no'])?$_GET['no']:'';
    }

    $form['gammu_number'] = array(
        '#type' => 'textarea',
        '#title' => t('Your mobile number'),
        '#rows' => 2,
        '#default_value' => $numero,
        '#description' => t("Enter your mobile number separated by comma (,)"),
    );
    $phonebook = gammu_get_phonebook();
    if(!empty($phonebook)){
        $form['gammu_phonebook'] = array(
            '#type' => 'fieldset',
            '#title' => t('Phone book'),
            '#collapsible' => TRUE,
            '#collapsed' => true,
        );
        $form['gammu_phonebook']['checkAll'] = array(
            '#type' => 'markup',
            '#markup' => '
            <div class="form-inline">
                <div class="form-group">
                    <input type="checkbox" id="checkAll" class="checkAll"/> <label for="checkAll">'.t('Check All').'</label>
                 </div>
                 <div class="form-group">
                    <input type="search" id="phonebook_search" class="phonebook_search form-control" placeholder="'.t('search').'"/>
                 </div>
             </div>',
        );
        $form['#attached']['js'][] = array(
            'type' => 'file',
            'data' => drupal_get_path('module','gammu') . '/gammu.js',
        );
        foreach ($phonebook as $num=>$name){
            $form['gammu_phonebook'][$num] = array(
                '#type' => 'checkbox',
                '#title' => $name
            );
        }
    }
    $form['#tree'] = true;
    $form['gammu_text'] = array(
        '#type' => 'textarea',
        '#title' => t('SMS Text'),
        '#rows' => 3,
        '#default_value' => '',
    );
    $form['gammu_submit'] = array(
        '#type' => 'submit',
        '#value' => t('Send SMS'),
        '#submit' => array('gammu_send_sms'),
    );
    return $form;
}

/*
 * compose sms form submit
 */
function gammu_send_sms($form, &$form_state){
    $numbers = !empty($form_state['input']['gammu_number']) ? explode(',',$form_state['input']['gammu_number']) : [];
    foreach ($form_state['input']['gammu_phonebook'] as $number=>$is_check){
        if($is_check){
            $numbers[] = $number;
        }
    }
    // will call all modules implementing hook_gammu_number_alter
    drupal_alter('gammu_number', $numbers);
    if(!empty($numbers)){
        foreach ($numbers as $number){
            $text = $form_state['input']['gammu_text'];
            // will call all modules implementing hook_gammu_text_alter
            drupal_alter('gammu_text', $number, $text);
            gammu_sendsms($number, $text);
        }
    }
    return true;
}

/*
 * get message multipart of folder
 */
function get_message_multipart($folder,$UDH,$num_mobile=''){

    gammu_connection();
    db_set_active('gammu_db');

    $udh = substr($UDH, 0, -2);
    $query = db_select($folder,'i');
    $query->fields('i', array('TextDecoded'));
    $query->condition('i.UDH', $udh.'%', 'like');
    if($folder=='sentitems'){
        $query->condition('i.DestinationNumber', $num_mobile)->orderBy('SequencePosition', 'ASC');
    }elseif($folder=='inbox'){
        $query->condition('i.UDH', $udh.'%', 'like') ->orderBy('ID', 'ASC');
    }
    $results = $query->execute()->fetchCol();
    db_set_active();
    return implode('',$results);
}

/*
 * delete message in folder
 */
function delete_message($folder,$ID){

    gammu_connection();
    db_set_active('gammu_db');

    $query_delete = db_delete($folder);
    if($folder =='inbox'){
        //detect if message is multipart in inbox
        $query = db_select($folder,'i');
        $query->fields('i', array('UDH'));
        $query->condition('i.ID', $ID);
        $results = $query->execute()->fetchCol();
        $UDH = end($results);
        if(trim($UDH) != ''){
            $udh = substr($UDH,0,-2);
            $query_delete->condition('UDH', $udh.'%', 'like') ;
            return $query_delete->execute();
        }
    }

    $query_delete->condition('ID', $ID);
    $num_delete = $query_delete->execute();

    db_set_active();
    return $num_delete;
}

/*
 *  submit form delete
 */
function gammu_delete($form, &$form_state){
    foreach (['sentitems','inbox'] as $folder){
        if(!empty($form_state['input'][$folder])){
            foreach (array_keys($form_state['input'][$folder]) as $ID){
                delete_message($folder,$ID);
            }
        }
    }
}

/*
 * Get all number of folder
 */
function gammu_getlist_num_phone($folder='sentitems'){
    $country_code = variable_get('gammu_country_code', '+33');
    gammu_connection();
    db_set_active('gammu_db');
    switch ($folder){
        case  'sentitems':
            $sqlTelephone = "SELECT DISTINCT DestinationNumber  FROM {sentitems} WHERE UDH = '' OR UDH LIKE '%1'";
            break;
        case   'inbox' :
            $sqlTelephone = "SELECT DISTINCT SenderNumber  FROM {inbox} WHERE UDH = '' OR UDH LIKE '%1'";
            break;
    }
    $telephones = db_query($sqlTelephone)->fetchCol();
    foreach ($telephones as &$telephone){
        $telephone = str_replace($country_code,0,$telephone);
    }
    db_set_active();
    return $telephones;
}

/*
 * mapping telephone num to name
 * input array of telephone number
 * sort : false (unapliquable), asc - ascending, desc -  descending
 * status : check / dont check status
 */
function gammu_get_phonebook($array_telephones=array(),$sort='asc' , $status=true){

    list($type_bundles,$field_name,$bundle)=explode('|',variable_get('gammu_telephone_field'));
    $list = false;
    if(!empty($bundle)){
        $query = db_select('field_data_'.$field_name, 'f')
            ->fields('f',['entity_id'])
            ->condition('f.entity_type', $type_bundles)
            ->condition('f.bundle', $bundle)
            ->groupBy('f.'.$field_name.'_value');
        if(!empty($array_telephones)){
            $query->condition('f.'.$field_name.'_value', $array_telephones, 'IN');
        }
        $nids = $query->execute()->fetchCol();
        $type = $type_bundles.'_load';
        $firstname_field = variable_get('gammu_firstname_field',FALSE);
        $lastname_field = variable_get('gammu_name_field',FALSE);
        if($firstname_field || $lastname_field){
            foreach ($nids as $nid){
                $bundle_load = $type($nid);
                //filter node or user active
                $active = 1;
                if($status && in_array($type_bundles,['user','node']) && !variable_get('gammu_check_status')){
                    $active = $bundle_load->status;
                }
                if(!empty($bundle_load) && $active){
                    $name = [];
                    $name[] = render(field_view_field($type_bundles, $bundle_load, $firstname_field)[0]);
                    $name[] = render(field_view_field($type_bundles, $bundle_load, $lastname_field)[0]);
                    $telephones = render(field_view_field($type_bundles, $bundle_load, $field_name)[0]);
                    $name = array_unique($name);
                    $list[$telephones] = trim(implode(' ',$name));
                }
            }
        }
        if(!empty($list) && $sort){
            if($sort == 'asc'){
                asort($list);
            }elseif($sort=='desc'){
                arsort($list);
            }
        }
    }
    return $list;
}

/*
 * read folder sentitems
 */
function gammu_sentitems($form, &$form_state){
    $array_telephones = gammu_getlist_num_phone('sentitems');
    $country_code = variable_get('gammu_country_code', '+33');
    $data_telephone = gammu_get_phonebook($array_telephones);
    gammu_connection();
    db_set_active('gammu_db');
    $sql = "SELECT DestinationNumber, ID, SendingDateTime, UDH, TextDecoded FROM {sentitems} WHERE UDH = '' OR UDH LIKE '%1' GROUP BY SendingDateTime ORDER BY SendingDateTime DESC";
    $results = db_query($sql);
    if(!empty($results)){
        $form['sentitems'] = array(
            '#type' => 'fieldset',
            '#title' => false,
        );
        $form['sentitems']['checkAll'] = array(
            '#type' => 'markup',
            '#markup' => '
                <div class="form-group">
                    <input type="checkbox" id="checkAll" class="checkAll"/> <label for="checkAll">'.t('Check All').'</label>
                 </div>',
        );
        $form['#attached']['js'][] = array(
            'type' => 'file',
            'data' => drupal_get_path('module','gammu') . '/gammu.js',
        );
        foreach ($results as $message){
            $TextDecoded = trim($message->UDH)!=''?get_message_multipart('sentitems',$message->UDH,$message->DestinationNumber):$message->TextDecoded;
            $phone_num = str_replace($country_code,0,$message->DestinationNumber);
            $phone_name = !empty($data_telephone[$phone_num])?$data_telephone[$phone_num]:$message->DestinationNumber;
            $form['sentitems']["sentitems[{$message->ID}]"] = array(
                '#type' => 'checkbox',
                '#default_value' => $message->ID,
                '#title' => $TextDecoded,
                '#description' => gammu_message_detail($message->DestinationNumber,$phone_name,$phone_num,$message->SendingDateTime),
                '#prefix' => '<div class="list-group-item">',
                '#suffix' => '</div>',
            );
        }
        $form['gammu_submit'] = array(
            '#type' => 'submit',
            '#value' => t('Delete'),
            '#submit' => array('gammu_delete'),
        );
    }

    db_set_active();

    $form_search = drupal_get_form('gammu_sms_search','sentitems');
    $form['#prefix'] = drupal_render($form_search);
    return $form;
}

/*
 * read folder sentitems
 */
function gammu_inbox($form, &$form_state){

    $array_telephones = gammu_getlist_num_phone('inbox');

    $data_telephone = gammu_get_phonebook($array_telephones);
    $country_code = variable_get('gammu_country_code', '+33');

    gammu_connection();
    db_set_active('gammu_db');
    $sql = "SELECT ID,ReceivingDateTime, SenderNumber , UDH , TextDecoded FROM {inbox} WHERE UDH = '' OR UDH LIKE '%1' GROUP BY SenderNumber ORDER BY ReceivingDateTime DESC";

    $results = db_query($sql);
    if(!empty($results)){
        $form['inbox'] = array(
            '#type' => 'fieldset',
            '#title' => false,
        );
        $form['inbox']['checkAll'] = array(
            '#type' => 'markup',
            '#markup' => '
                <div class="form-group">
                    <input type="checkbox" id="checkAll" class="checkAll"/> <label for="checkAll">'.t('Check All').'</label>
                 </div>',
        );
        $form['#attached']['js'][] = array(
            'type' => 'file',
            'data' => drupal_get_path('module','gammu') . '/gammu.js',
        );
        foreach ($results as $message){
            $TextDecoded = trim($message->UDH)!=''?get_message_multipart('inbox',$message->UDH):$message->TextDecoded;
            $phone_num = str_replace($country_code,0,$message->SenderNumber);
            $phone_name = !empty($data_telephone[$phone_num])?$data_telephone[$phone_num]:$message->SenderNumber;
            $form['inbox']["inbox[{$message->ID}]"] = array(
                '#type' => 'checkbox',
                '#default_value' => $message->ID,
                '#title' => $TextDecoded,
                '#description' => gammu_message_detail($message->SenderNumber,$phone_name,$phone_num,$message->ReceivingDateTime),
                '#prefix' => '<div class="list-group-item">',
                '#suffix' => '</div>',
            );
        }
        $form['gammu_submit'] = array(
            '#type' => 'submit',
            '#value' => t('Delete'),
            '#submit' => array('gammu_delete'),
        );
    }

    db_set_active();
    $form_search = drupal_get_form('gammu_sms_search', 'inbox');
    $form['#prefix'] = drupal_render($form_search);
    return $form;
}

/*
 * read all conversation
 */
function gammu_conversation( $form, &$form_state,$numero=''){

    $country_code = variable_get('gammu_country_code', '+33');
    $phone_num = $numero ;

    $numero = str_replace($country_code,'',$numero);
    $data_telephone = gammu_get_phonebook([$numero]);

    $phone_name = !empty($data_telephone[$phone_num])?$data_telephone[$phone_num]:$phone_num;
    drupal_set_title($phone_name);

    gammu_connection();
    db_set_active('gammu_db');

    $sql_sentitems = "SELECT  ID, SendingDateTime as time, DestinationNumber as Number,UDH, TextDecoded,'sentitems' as folder FROM {sentitems} WHERE (UDH = '' OR UDH LIKE '%1') AND DestinationNumber like '%$numero' ";

    $numero = ltrim($numero,0);
    $sql_inbox = "SELECT ID,ReceivingDateTime as time, SenderNumber as Number, UDH , TextDecoded, 'inbox' as folder FROM {inbox} WHERE (UDH = '' OR UDH LIKE '%1') AND SenderNumber like '%$numero'";

    $query = $sql_sentitems.' UNION ALL '.$sql_inbox .' ORDER BY time';

    $arrayMessage = db_query($query)->fetchAll(PDO::FETCH_ASSOC);

    db_set_active();
    if(!empty($arrayMessage)){
        foreach ($arrayMessage as $message){
            $TextDecoded = trim($message['UDH'])!=''?get_message_multipart($message['folder'],$message['UDH'],$phone_num):$message['TextDecoded'];

            $class = ['panel', $message['folder'],'col-md-11'];
            if($message['folder'] == 'inbox'){
                $class[] = 'panel-success';
                $class[] = 'bg-success';
            }else{
                $class[] = 'panel-info';
                $class[] = 'bg-info';
                $class[] = 'col-md-offset-1';
            }
            $form[$message['folder']]["{$message['folder']}[{$message['ID']}]"] = array(
                '#type' => 'checkbox',
                '#default_value' => $message['ID'],
                '#description' =>gammu_nice_date($message['time']),
                '#title' => $TextDecoded,
                '#prefix' => '<div class="'.implode(' ',$class).'"><div class="panel-body">',
                '#suffix' => '</div></div>',
            );
        }
        $form['gammu_submit'] = array(
            '#type' => 'submit',
            '#value' => t('Delete'),
            '#submit' => array('gammu_delete'),
            '#prefix' => '<div class="col-md-12">',
            '#suffix' => '</div>',
        );
    }

    $form_search = drupal_get_form('gammu_sms_search',true);
    $form['#prefix'] = drupal_render($form_search);
    return $form;
}

/*
 * Helper function for gammu
 */
function gammu_nice_date($date){
    $date = strtotime($date);
    $interval = time() - $date;
    //show time ago
    if($interval < 60*60*24*2){
        return format_interval($interval);
    }
    return format_date($date,'short');
}

function gammu_message_detail($Number,$phone_name,$phone_num,$time){
    return
        '<span data-toggle="tooltip" title="'.$Number.'">'.$phone_name. '</span> 
        <span class="btn-group"> 
            <a class="btn-xs btn-success" href="/admin/sms/compose/'.$phone_num.'"><i class="glyphicon glyphicon-share-alt"></i> Reply</a> 
            <a class="btn-xs btn-info" href="/admin/sms/conversation/'.$phone_num.'"><i class="glyphicon glyphicon-inbox"></i> Detail</a> 
        </span> 
        <time class="text-right" datetime="'.$time.'"> ' .gammu_nice_date($time).'</time>';
}

/*
 * Get List code dial iso
 */
function getCountryInformation($filter = '') {

    $countryArray = [
'+376' => "AD",
'+971' => "AE",
'+93' => "AF",
'+1268' => "AG",
'+1264' => "AI",
'+355' => "AL",
'+374' => "AM",
'+599' => "AN",
'+244' => "AO",
'+672' => "AQ",
'+54' => "AR",
'+1684' => "AS",
'+43' => "AT",
'+61' => "AU",
'+297' => "AW",
'+994' => "AZ",
'+387' => "BA",
'+1246' => "BB",
'+880' => "BD",
'+32' => "BE",
'+226' => "BF",
'+359' => "BG",
'+973' => "BH",
'+257' => "BI",
'+229' => "BJ",
'+590' => "BL",
'+1441' => "BM",
'+673' => "BN",
'+591' => "BO",
'+55' => "BR",
'+1242' => "BS",
'+975' => "BT",
'+267' => "BW",
'+375' => "BY",
'+501' => "BZ",
'+1' => "CA",
'+61' => "CC",
'+243' => "CD",
'+236' => "CF",
'+242' => "CG",
'+41' => "CH",
'+225' => "CI",
'+682' => "CK",
'+56' => "CL",
'+237' => "CM",
'+86' => "CN",
'+57' => "CO",
'+506' => "CR",
'+53' => "CU",
'+238' => "CV",
'+61' => "CX",
'+357' => "CY",
'+420' => "CZ",
'+49' => "DE",
'+253' => "DJ",
'+45' => "DK",
'+1767' => "DM",
'+1809' => "DO",
'+213' => "DZ",
'+593' => "EC",
'+372' => "EE",
'+20' => "EG",
'+291' => "ER",
'+34' => "ES",
'+251' => "ET",
'+358' => "FI",
'+679' => "FJ",
'+500' => "FK",
'+691' => "FM",
'+298' => "FO",
'+33' => "FR",
'+241' => "GA",
'+44' => "GB",
'+1473' => "GD",
'+995' => "GE",
'+233' => "GH",
'+350' => "GI",
'+299' => "GL",
'+220' => "GM",
'+224' => "GN",
'+240' => "GQ",
'+30' => "GR",
'+502' => "GT",
'+1671' => "GU",
'+245' => "GW",
'+592' => "GY",
'+852' => "HK",
'+504' => "HN",
'+385' => "HR",
'+509' => "HT",
'+36' => "HU",
'+62' => "ID",
'+353' => "IE",
'+972' => "IL",
'+44' => "IM",
'+91' => "IN",
'+964' => "IQ",
'+98' => "IR",
'+354' => "IS",
'+39' => "IT",
'+1876' => "JM",
'+962' => "JO",
'+81' => "JP",
'+254' => "KE",
'+996' => "KG",
'+855' => "KH",
'+686' => "KI",
'+269' => "KM",
'+1869' => "KN",
'+850' => "KP",
'+82' => "KR",
'+965' => "KW",
'+1345' => "KY",
'+7' => "KZ",
'+856' => "LA",
'+961' => "LB",
'+1758' => "LC",
'+423' => "LI",
'+94' => "LK",
'+231' => "LR",
'+266' => "LS",
'+370' => "LT",
'+352' => "LU",
'+371' => "LV",
'+218' => "LY",
'+212' => "MA",
'+377' => "MC",
'+373' => "MD",
'+382' => "ME",
'+1599' => "MF",
'+261' => "MG",
'+692' => "MH",
'+389' => "MK",
'+223' => "ML",
'+95' => "MM",
'+976' => "MN",
'+853' => "MO",
'+1670' => "MP",
'+222' => "MR",
'+1664' => "MS",
'+356' => "MT",
'+230' => "MU",
'+960' => "MV",
'+265' => "MW",
'+52' => "MX",
'+60' => "MY",
'+258' => "MZ",
'+264' => "NA",
'+687' => "NC",
'+227' => "NE",
'+234' => "NG",
'+505' => "NI",
'+31' => "NL",
'+47' => "NO",
'+977' => "NP",
'+674' => "NR",
'+683' => "NU",
'+64' => "NZ",
'+968' => "OM",
'+507' => "PA",
'+51' => "PE",
'+689' => "PF",
'+675' => "PG",
'+63' => "PH",
'+92' => "PK",
'+48' => "PL",
'+508' => "PM",
'+870' => "PN",
'+1' => "PR",
'+351' => "PT",
'+680' => "PW",
'+595' => "PY",
'+974' => "QA",
'+40' => "RO",
'+381' => "RS",
'+7' => "RU",
'+250' => "RW",
'+966' => "SA",
'+677' => "SB",
'+248' => "SC",
'+249' => "SD",
'+46' => "SE",
'+65' => "SG",
'+290' => "SH",
'+386' => "SI",
'+421' => "SK",
'+232' => "SL",
'+378' => "SM",
'+221' => "SN",
'+252' => "SO",
'+597' => "SR",
'+239' => "ST",
'+503' => "SV",
'+963' => "SY",
'+268' => "SZ",
'+1649' => "TC",
'+235' => "TD",
'+228' => "TG",
'+66' => "TH",
'+992' => "TJ",
'+690' => "TK",
'+670' => "TL",
'+993' => "TM",
'+216' => "TN",
'+676' => "TO",
'+90' => "TR",
'+1868' => "TT",
'+688' => "TV",
'+886' => "TW",
'+255' => "TZ",
'+380' => "UA",
'+256' => "UG",
'+1' => "US",
'+598' => "UY",
'+998' => "UZ",
'+39' => "VA",
'+1784' => "VC",
'+58' => "VE",
'+1284' => "VG",
'+1340' => "VI",
'+84' => "VN",
'+678' => "VU",
'+681' => "WF",
'+685' => "WS",
'+967' => "YE",
'+262' => "YT",
'+27' => "ZA",
'+260' => "ZM",
'+263' => "ZW",
];
    //Return
    return ( $filter == '' ) ? $countryArray : (isset($countryArray[$filter]) ? $countryArray[$filter] : '');
}
/*
 * covert dialcode to name of country
 */
function getCountryDialCode() {
    $countryArray = getCountryInformation();
    $list = country_get_list();
    foreach($countryArray as $key => $country)
    {
       if(!empty($list[$country])) $dialCode[$key] = $list[$country].' ('.$key.')';
    }
    return $dialCode;
}

function gammu_sms_search($form, &$form_state,$folder = false){
    $form['gammu_sms_search'] = array(
        '#type' => 'textfield',
        '#required' => FALSE,
        '#attributes'=>['placeholder' => t('Search')],
        '#prefix' => '<div class="col-md-10">',
        '#suffix' => '</div>',
    );
    $form['gammu_sms_search_submit'] = array(
        '#type' => 'submit',
        '#id' => 'gammu_search',
        '#value' => t('Search'),
    );
    $form['#action'] = '/'.GAMMU_SMS_SEARCH;

    if(!empty($form_state['input']['gammu_sms_search'])) {
        $_SESSION['gammu_sms_search'] = $form_state['input']['gammu_sms_search'];
    }elseif(!empty($_SESSION['gammu_sms_search']) && !$folder) {
        $form['gammu_sms_search']['#default_value'] = $_SESSION['gammu_sms_search'];
        $gammu_sms_search = $_SESSION['gammu_sms_search'];

        $search ='%'.db_like($gammu_sms_search).'%';

        //check if search by numero telephone remove country code and 0
        $country_code = variable_get('gammu_country_code', '+33');
        $temp = str_replace($country_code,'',$gammu_sms_search);
        if(is_numeric(ltrim($temp, 0) )){
            $search = '%'.ltrim($temp, 0).'%';
        }

        gammu_connection();
        db_set_active('gammu_db');
        $sql_sentitems = "SELECT DestinationNumber as Number, ID, SendingDateTime as time,UDH, TextDecoded,'sentitems' as folder FROM {sentitems} WHERE (UDH = '' OR UDH LIKE '%1') AND (DestinationNumber like '$search' OR TextDecoded  like '$search') ";

        $sql_inbox = "SELECT SenderNumber as Number, ID,ReceivingDateTime as time, UDH , TextDecoded, 'inbox' as folder FROM {inbox} WHERE (UDH = '' OR UDH LIKE '%1') AND (SenderNumber like '$search' OR TextDecoded  like '$search') ";

        $number_telephone =db_query($sql_sentitems.' UNION ALL '.$sql_inbox .' Group by Number')->fetchCol();

        $query = $sql_sentitems.' UNION ALL '.$sql_inbox .' ORDER BY time';
        $results = db_query($query);
        db_set_active();
        $data_telephone = gammu_get_phonebook($number_telephone);
        if (!empty($results)) {

            foreach ($results as $message) {
                $time = strtotime($message->time);
                $TextDecoded = trim($message->UDH)!=''?get_message_multipart($message->folder,$message->UDH,$message->Number):$message->TextDecoded;

                $class = ['panel', $message->folder,'col-md-11'];
                if($message->folder == 'inbox'){
                    $class[] = 'panel-success';
                    $class[] = 'bg-success';
                }else{
                    $class[] = 'panel-info';
                    $class[] = 'bg-info';
                    $class[] = 'col-md-offset-1';
                }
                if(empty($form[$message->folder])){
                    $form[$message->folder] = array(
                        '#type' => 'fieldset',
                        '#title' => t($message->folder),
                        '#collapsible' => TRUE,
                        '#collapsed' => false,
                    );
                    $form[$message->folder]['checkAll'] = array(
                        '#type' => 'markup',
                        '#markup' => '
            <div class="form-inline">
                <div class="form-group">
                    <input type="checkbox" id="checkAll" class="checkAll"/> <label for="checkAll">'.t('Check All').'</label>
                 </div>
             </div>',
                    );
                    $form['#attached']['js'][] = array(
                        'type' => 'file',
                        'data' => drupal_get_path('module','gammu') . '/gammu.js',
                    );
                }
                $message->Number = str_replace($country_code,0,$message->Number);
                $phone_name = !empty($data_telephone[$message->Number])?$data_telephone[$message->Number]:$message->Number;
                $form[$message->folder]["{$message->folder}[{$message->ID}]"] = array(
                    '#type' => 'checkbox',
                    '#default_value' => $message->ID,
                    '#description' => gammu_message_detail($message->Number,$phone_name,$message->Number,$message->time),
                    '#title' => $TextDecoded,
                    '#prefix' => '<div class="'.implode(' ',$class).'"><div class="panel-body">',
                    '#suffix' => '</div></div>',
                );
            }
        }
    }
    if(!$folder){
        $form['gammu_submit_delete'] = array(
            '#type' => 'submit',
            '#value' => t('Delete'),
            '#prefix' => '<div class="col-md-12">',
            '#suffix' => '</div>',
        );
    }

    return $form;
}
function gammu_sms_search_submit($form, &$form_state){
    if($form_state['values']['op'] == $form_state['values']['gammu_submit_delete'])
    {
        gammu_delete($form, $form_state);
    }
}