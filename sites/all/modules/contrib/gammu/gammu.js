(function($, Drupal) {

    Drupal.behaviors.gammu = {
        attach:function() {
            $("#checkAll").change(function () {
                $(this).closest('fieldset').find(".form-type-checkbox:visible input:checkbox").prop('checked', $(this).prop("checked"));
            });

            $(".phonebook_search").keyup(function () {
                var txt = $(this).val();
                $(this).closest('fieldset').find('.form-type-checkbox').hide();
                $(this).closest('fieldset').find('.form-type-checkbox').each(function () {
                    if ($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1) {
                        $(this).show();
                    }
                });
            });
        }
    };
}(jQuery, Drupal));