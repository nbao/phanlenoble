<?php if (!empty($title)): ?>
    <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
    <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
        <?php print $row; ?>
    </div>

<?php endforeach;

$nid =0;
if (arg(0) == 'node' && is_numeric(arg(1))) {
    $nid = arg(1);
}
module_load_include('inc', 'node', 'node.pages');
$node=node_load($nid);
if(empty($node->field_signature_assure)){
    $form=drupal_get_form($node->type . '_node_form', $node);
    $field=array(
        'field_signature_assure'=>$form['field_signature_assure'],
        '#method'=>$form['#method'],
        '#theme_wrappers'=>$form['#theme_wrappers'],
        '#action'=>$form['#action'].'/signature?destination='.$form['#action'],
        '#submit'=>$form['#submit'],
    );
    $bt_action=$form['actions'];
    $form=$field;
    $form['actions']=$bt_action;
    $form['actions']['delete']['#access'] = FALSE;

    $signature = drupal_render($form);
    echo $signature;
}