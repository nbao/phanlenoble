/**
 * Created by baonguyen on 11/11/14.
 */

(function ($) {
    Drupal.behaviors.panel = {
        attach: function (context, settings) {
            $('.field-group-table.panel label.control-label').parent().addClass('panel-heading');
            $('.field-group-table.panel .panel-heading').click(function() {
            	$(this).closest('.panel').find('.table-responsive').toggle('slow');
            });
            $('#group-scan .panel-heading').click(function() {
            	$(this).closest('.panel').find('.panel-body').toggle('slow');
            });
	        $( ".view-id-planing .form-checkbox,.view-id-planing .form-text" ).on( "change", function() {
	            var post=$(this).data();
	            	post.value = 0;
			    if ($(this).is(":checked")) {
			        post.value=1;
			    }
				if($(this).attr('type')=='text'){
                    post.value = $(this).val();
                }
	            $.ajax({
					url:'/planning',
					type:"POST",
					data:post,
					success:function(return_data){
						//console.log(return_data);
					},
					error:function(jqXHR, textStatus, errorMessage){
						//error($("#notification"),errorMessage);
					}
	            });
	        });


	        $( ".form-checkbox.rappel-telephone" ).on( "change", function() {
	            var post=$(this).data();
	            	post.value = 0; 
			    if ($(this).is(":checked")) {
			        post.value=1;
			    }
	            $.ajax({
					url:'/rappel',
					type:"POST",
					data:post,
					success:function(return_data){
						console.log(return_data);
					},
					error:function(jqXHR, textStatus, errorMessage)
					{
						//error($("#notification"),errorMessage);
					}
				});
	        });
        }
    };
})(jQuery);
