<?php

/**
 * @file
 * template.php
 */

function phanlenoble_menu_tree__menu_footer($variables) {
  return '<ul class="nav nav-pills">' . $variables['tree'] . '</ul>';
}

function phanlenoble_password($variables) {
 $element = $variables['element'];
 $element['#attributes']['type'] = 'password';
 element_set_attributes($element, array('id', 'name', 'size', 'maxlength'));
 _form_set_class($element, array('form-text', 'form-control'));
 
 $output = '';
 // login form adding glyphicon.
 if($element['#name'] == 'pass')
  $output = '<div class="input-group"><span class="input-group-addon"><span class="glyphicon glyphicon-eye-close"></span></span>';
 
 $output .= '<input' . drupal_attributes($element['#attributes']) . ' />';
 if($element['#name'] == 'pass') 
  $output .= '</div>';
 return $output;
}

function phanlenoble_entityreference_view_widget_rows($variables) {
    $zebra = array('even' => 'odd');

    $settings = $variables['rows']['#widget_settings'];
    foreach (element_children($variables['rows']) as $row) {
        $output[$row]  = '<tr class="draggable ' . current($zebra) . '">';
        $output[$row] .= '<td class="field-multiple-drag"></td>';
        if(isset($variables['rows'][$row][$settings['column']]['#field_suffix'])){
            $variables['rows'][$row][$settings['column']]['#suffix']=$variables['rows'][$row][$settings['column']]['#field_suffix'];
        }
        $output[$row] .= '<td>' . render($variables['rows'][$row][$settings['column']]) . '</td>';
        if ($settings['cardinality'] !== '1') {
            $output[$row] .= '<td class="delta-order tabledrag-hide">' . render($variables['rows'][$row]['_weight']) . '</td>';
        }
        $output[$row] .= '</tr>';

        $zebra = array_flip($zebra);
    }

    return implode("\n", $output);
}
function phanlenoble_preprocess_views_view(&$vars) {
    // Get the current view info
    $view = $vars['view'];

    // Add JS/CSS based on view name
    if ($view->name == 'sms' and $view->current_display == 'essaie') {
        drupal_add_js(drupal_get_path('module', 'orthese') . '/js/sms.js');
    }
}

/**
 * Implementation of hook_preprocess_html().
 */
function phanlenoble_preprocess_html(&$variables) {
  global $language;
  $css_customizer = theme_get_setting('skin') . '-pages-customizer.css';
  drupal_add_css(drupal_get_path('theme', $GLOBALS['theme']) . '/css/customizer/' . $css_customizer, array('group' => CSS_THEME));
  if($language->dir == 'rtl') {
    drupal_add_css(drupal_get_path('theme', $GLOBALS['theme']) . '/css/drupal-rtl.css', array('group' => CSS_THEME));
  }
  drupal_add_js(array(
    'theme_path' => drupal_get_path('theme', $GLOBALS['theme']),
    'basePath' => base_path(),
    'progressive' => array(
      'mobile_menu_toggle' => theme_get_setting('mobile_menu_toggle')
    ),
    'ubercart_currency' => variable_get('uc_currency_sign')
  ), 'setting');
  // Page 404
  if(arg(0) == 'page-404-bg') {
    $variables['classes_array'][] = 'page-404-promo';
  }
}
/**
 * Implementation of hook_preprocess_page().
 */
function phanlenoble_preprocess_page(&$vars, $hook) {
  global $user;
  if (arg(0) == 'user' && (arg(1) == 'register' || arg(1) == 'password' || (arg(1) == '' && !$user->uid))) {
    $vars['theme_hook_suggestions'][] = 'page__user__login';
  }
}

/**
 * Implements theme_field()
 */
function phanlenoble_field__field_social_icons($variables) {
  $output = '';
  if(count($variables['items'])) {
    $output .= '<div class = "social">';
    for ($i = 0; $i < count($variables['items']); $i++) {
      //dpm($variables);
      $output .= '<div class = "item"><a class="sbtnf sbtnf-rounded color color-hover ' . $variables['items'][$i]['#element']['title'] . '" href="' . $variables['items'][$i]['#element']['url'] . '" target = "_blank"></a></div>';
    }
    $output .= '</div>';
  }
  return $output;
}
