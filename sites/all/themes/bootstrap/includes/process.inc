<?php

/**
 * @file
 * process.inc
 *
 * Contains various implementations for #process callbacks on elements.
 */

/**
 * Implements hook_form_process().
 */
function bootstrap_form_process($element, &$form_state, &$form) {
  if (!empty($element['#bootstrap_ignore_process'])) {
    return $element;
  }

  if (!empty($element['#attributes']['class']) && is_array($element['#attributes']['class'])) {
    $key = array_search('container-inline', $element['#attributes']['class']);
    if ($key !== FALSE) {
      $element['#attributes']['class'][$key] = 'form-inline';
    }

    if (in_array('form-wrapper', $element['#attributes']['class'])) {
      $element['#attributes']['class'][] = 'form-group';
    }
  }

  // Automatically inject the nearest button found after this element if
  // #input_group_button exists.
  if (!empty($element['#input_group_button'])) {
    // Obtain the parent array to limit search.
    $array_parents = array();
    if (!empty($element['#array_parents'])) {
      $array_parents += $element['#array_parents'];
      // Remove the current element from the array.
      array_pop($array_parents);
    }

    // If element is nested, return the referenced parent from the form.
    if (!empty($array_parents)) {
      $parent = &drupal_array_get_nested_value($form, $array_parents);
    }
    // Otherwise return the complete form.
    else {
      $parent = &$form;
    }

    // Ignore buttons before we find the element in the form.
    $found_current_element = FALSE;
    foreach (element_children($parent, TRUE) as $child) {
      if ($parent[$child] === $element) {
        $found_current_element = TRUE;
        continue;
      }

      if ($found_current_element && _bootstrap_is_button($parent[$child]) && (!isset($parent[$child]['#access']) || !!$parent[$child]['#access'])) {
        $element['#field_suffix'] = drupal_render($parent[$child]);
        break;
      }
    }
  }

  return $element;
}

/**
 * Implements hook_form_process_HOOK().
 */
function bootstrap_form_process_actions($element, &$form_state, &$form) {
  $element['#attributes']['class'][] = 'form-actions';

  if (!empty($element['#bootstrap_ignore_process'])) {
    return $element;
  }

  foreach (element_children($element) as $child) {
    _bootstrap_iconize_button($element[$child]);
  }
  return $element;
}

/**
 * Implements hook_form_process_HOOK().
 */
function bootstrap_form_process_text_format($element, &$form_state, &$form) {
  if (!empty($element['#bootstrap_ignore_process'])) {
    return $element;
  }

  // Provide smart description on the value text area.
  bootstrap_element_smart_description($element, $element['value']);

  // Allow the elements inside to be displayed inline.
  $element['format']['#attributes']['class'][] = 'form-inline';

  // Remove the cluttering guidelines; they can be viewed on a separate page.
  $element['format']['guidelines']['#access'] = FALSE;

  // Hide the select label.
  $element['format']['format']['#title_display'] = 'none';

  // Make the select element smaller using a Bootstrap class.
  $element['format']['format']['#attributes']['class'][] = 'input-sm';

  // Support the Bootstrap Select plugin if it is used.
  $element['format']['format']['#attributes']['data-style'] = 'btn-sm btn-default';

  return $element;
}

/*
 * Implements hook_theme status_report
 */
function bootstrap_status_report($variables)
{
    $requirements = $variables['requirements'];
    $severities = array(
        REQUIREMENT_INFO => array(
            'title' => t('Info'),
            'class' => 'info',
        ),
        REQUIREMENT_OK => array(
            'title' => t('OK'),
            'class' => 'ok',
        ),
        REQUIREMENT_WARNING => array(
            'title' => t('Warning'),
            'class' => 'warning',
        ),
        REQUIREMENT_ERROR => array(
            'title' => t('Error'),
            'class' => 'error',
        ),
    );
    $output = '<div class="table-responsive"><table class="table table-hover table-striped sticky-enabled system-status-report">';

    foreach ($requirements as $requirement) {
        if (empty($requirement['#type'])) {
            $severity = $severities[isset($requirement['severity']) ? (int)$requirement['severity'] : REQUIREMENT_OK];
            $severity['icon'] = '<div title="' . $severity['title'] . '"><span class="element-invisible">' . $severity['title'] . '</span></div>';
            // The requirement's 'value' key is optional, provide a default value.
            $requirement['value'] = isset($requirement['value']) ? $requirement['value'] : '';
            $severity['class'] = str_replace('ok', 'success', $severity['class']);
            // Output table row(s)
            if (!empty($requirement['description'])) {
                $output .= '<tr class="' . $severity['class'] . ' merge-down"><td class="status-icon">' . $severity['icon'] . '</td><td class="status-title">' . $requirement['title'] . '</td><td class="status-value">' . $requirement['value'] . '</td></tr>';
                $output .= '<tr class="' . $severity['class'] . ' merge-up"><td colspan="3" class="status-description">' . $requirement['description'] . '</td></tr>';
            } else {
                $output .= '<tr class="' . $severity['class'] . '"><td class="status-icon">' . $severity['icon'] . '</td><td class="status-title">' . $requirement['title'] . '</td><td class="status-value">' . $requirement['value'] . '</td></tr>';
            }
        }
    }

    $output .= '</table></div>';
    return $output;
}


/**
 * Implements theme_admin_block().
 * Adding classes to the administration blocks
 */
function bootstrap_admin_block($variables)
{
    $block = $variables['block'];
    $output = '';
    $icon = [
        'workflow' => 'glyphicon glyphicon-retweet',
        'messaging' => 'glyphicon glyphicon-envelope',
        'group' => 'fa fa-users',
        'services' => 'glyphicon glyphicon-signal',
        'development' => 'fa fa-bug',
        'user-interface' => 'glyphicon glyphicon-hand-up',
        'system' => 'glyphicon glyphicon-cog',
        'administration' => 'glyphicon glyphicon-wrench',
        'regional' => 'glyphicon glyphicon-globe',
        'search' => 'glyphicon glyphicon-search',
        'media' => 'glyphicon glyphicon-picture',
        'date' => 'glyphicon glyphicon-calendar',
        'content' => 'glyphicon glyphicon-pencil',
        'people' => 'glyphicon glyphicon-user',
        'fields' => 'glyphicon glyphicon-th-list',
        'elasticsearch-connector' => 'glyphicon glyphicon-list-alt'
    ];
    // Don't display the block if it has no content to display.
    if (empty($block['show'])) {
        return $output;
    }
    $class_tilte = $title = $href = '';
    $output .= '<div class="panel panel-default">';
    if (!empty($block['path'])) {
        $class_tilte = check_plain(str_replace(array("admin", "/"), " ", $block['path']));
        $href = check_plain(str_replace("/", "-", $block['path']));
    }
    $temp = explode(' ', $class_tilte);
    $icon_key = end($temp);
    $icon_val = !empty($icon[$icon_key]) ? $icon[$icon_key] : $icon['system'];
    $icon_string = "<i class='{$icon_val}'></i>";
    if (!empty($block['title'])) {
        $title = $block['title'];
    }
    $output .= '<a class="panel-heading ' . $class_tilte . '" data-toggle="collapse" href="#' . $href . '">' . $icon_string . ' ' . $title . ' <span class="caret"></span></a>';

    if (!empty($block['content'])) {
        $output .= '<div class="body panel-body" id="' . $href . '">' . $block['content'] . '</div>';
    } else {
        $output .= '<div class="description">' . $block['description'] . '</div>';
    }

    $output .= '</div>';

    return $output;
}

function bootstrap_admin_block_content($variables)
{
    $content = $variables['content'];
    $output = '';

    if (!empty($content)) {
        $class = 'admin-list ';
        if ($compact = system_admin_compact_mode()) {
            $class .= ' compact';
        }
        $output .= '<dl class="' . $class . '">';
        $i = 1;
        foreach ($content as $item) {
            $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
            if (!$compact && isset($item['description'])) {
                $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
            }
            if (count($content) != $i++) $output .= '<hr/>';
        }
        $output .= '</dl>';
    }
    return $output;
}